import json
import requests
from flask import Flask, render_template, request, jsonify
import re

app = Flask(__name__)

def extraer_hechos(texto):
    frases = re.split(r'[.!?y]', texto)
    frases = [frase.strip() for frase in frases if frase.strip()]
    frases_delimitadas = ["- " + frase for frase in frases]
    return frases_delimitadas

def obtener_respuestas(hechos, prompt):
    respuestas = []
    headers = {
        "authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiZWQ2NjE3MmUtZWRkMS00OTc1LWE5OTAtOTJiN2Y5N2NkOWNhIiwidHlwZSI6ImFwaV90b2tlbiJ9.MkOstsxwVlvp1XyA8cO7I_EY9X_QmkGF6PQcDy2HVKE"
    }
    url = "https://api.edenai.run/v2/text/chat"
    for hecho in hechos:
        payload = {
            "providers": "openai",
            "text": hecho,
            "chatbot_global_action": prompt,
            "previous_history": [],
            "temperature": 0.0,
            "max_tokens": 150
        }
        response = requests.post(url, json=payload, headers=headers)
        if response.status_code == 200:
            result = json.loads(response.text)
            respuesta = result['openai']['generated_text']
            respuestas.append(respuesta)
        else:
            respuestas.append(f"Error en la solicitud para el hecho: {hecho}")
    return respuestas

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/informacion')
def informacion():
    return render_template('informacion.html')

@app.route('/extraer_hechos', methods=['POST'])
def extraer_hechos_route():
    data = request.get_json()
    texto = data['texto']
    prompt = data['prompt']
    hechos = extraer_hechos(texto)
    opiniones = obtener_respuestas(hechos, prompt)
    return jsonify({'hechos': hechos, 'opiniones': opiniones})

if __name__ == '__main__':
    app.run(debug=True)
